---
dates: "November 7-9, 2012"
place: "Barcelona, Spain"
layout: archive
---
The schedule and slides are available on [linux-kvm.org](https://linux-kvm.org/page/KVM_Forum_2012).
