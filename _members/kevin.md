---
name: Kevin Wolf
role: Senior Principal Software Engineer - Red Hat
---
Kevin Wolf works at Red Hat as a KVM developer, with a focus on
block devices. He is the maintainer of QEMU's block subsystem and has
contributed many patches to block device emulation and image format
drivers. After graduating in Software Engineering at the University of
Stuttgart, Germany in 2008 he worked on Xen's block layer for a year
before he started working on KVM for Red Hat in 2009.
