---
name: Karen Noel
role: Senior Director, Core Platforms - Red Hat
---
Karen is the director for the Virtualization, Networking, File Systems and
Storage teams in Red Hat's Core Platforms department, which is responsible
for developing Red Hat Enterprise Linux (RHEL) and Fedora. Her team of
developers contributes to a wide variety of technologies in the Linux
kernel as well as in userspace, including the KVM hypervisor, QEMU,
Libvirt, as well other KVM related open source projects.

As an engineer for Digital and HP, Karen worked on
operating system kernels and the Integrity VM hypervisor (Intel IA-64)
and joined Red Hat as an engineering manager in 2011. Karen holds a BS
in Computer Science from Cornell University and an MBA degree from the
University of New Hampshire.
